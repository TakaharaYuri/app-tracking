webpackJsonp([0],{

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimelineComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return TimelineItemComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return TimelineTimeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TimelineComponent = /** @class */ (function () {
    function TimelineComponent() {
        this.endIcon = "ionic";
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('endIcon'),
        __metadata("design:type", Object)
    ], TimelineComponent.prototype, "endIcon", void 0);
    TimelineComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'timeline',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\components\timeline\timeline.html"*/'<div class="timeline">\n  <ng-content></ng-content>\n\n  <timeline-item>\n    <ion-icon class="" [name]="endIcon"></ion-icon>\n  </timeline-item>\n\n</div>\n'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\components\timeline\timeline.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TimelineComponent);
    return TimelineComponent;
}());

var TimelineItemComponent = /** @class */ (function () {
    function TimelineItemComponent() {
    }
    TimelineItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'timeline-item',
            template: '<ng-content></ng-content>'
        }),
        __metadata("design:paramtypes", [])
    ], TimelineItemComponent);
    return TimelineItemComponent;
}());

var TimelineTimeComponent = /** @class */ (function () {
    function TimelineTimeComponent() {
        this.time = {};
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('time'),
        __metadata("design:type", Object)
    ], TimelineTimeComponent.prototype, "time", void 0);
    TimelineTimeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'timeline-time',
            template: '<span></span>'
        }),
        __metadata("design:paramtypes", [])
    ], TimelineTimeComponent);
    return TimelineTimeComponent;
}());

//# sourceMappingURL=timeline.js.map

/***/ }),

/***/ 166:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 166;

/***/ }),

/***/ 215:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 215;

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_tabs_tabs__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(nav, alertCtrl, loadingCtrl, authService, storage, platform) {
        var _this = this;
        this.nav = nav;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.storage = storage;
        platform.ready().then(function () {
            _this.email = localStorage.getItem('email');
            _this.password = localStorage.getItem('password');
            if (_this.storage.get('isLogged')) {
                _this.login();
            }
        });
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    LoginPage.prototype.presentUrlConfigModal = function () {
        var alert = this.alertCtrl.create({
            title: 'Server URL',
            inputs: [
                {
                    name: 'url',
                    value: this.url,
                    placeholder: 'Enter Server URL'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        localStorage.setItem('url', data.url);
                    }
                }
            ]
        });
        alert.present();
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        console.log('Login');
        // this.showLoading();
        this.authService.postData(this.email, this.password, 'login').then(function (result) {
            _this.responseData = result;
            localStorage.setItem('email', _this.email);
            localStorage.setItem('password', _this.password);
            _this.storage.set('isLogged', true);
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_tabs_tabs__["a" /* TabsPage */]);
            _this.nav.popToRoot();
        }, function (err) {
            // this.showError(JSON.stringify(err));
        });
    };
    LoginPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: true
        });
        this.loading.present();
    };
    LoginPage.prototype.showError = function (text) {
        this.loading.dismiss();
        var alert = this.alertCtrl.create({
            title: 'Erro Loco',
            subTitle: JSON.stringify(text),
            buttons: ['OK']
        });
        alert.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\login\login.html"*/'<ion-content class="login-content" padding>\n\n    <!--<div style="width: 100%; text-align:right;">\n\n    <button ion-button icon-only  item-right (click)="presentUrlConfigModal()">\n\n        <ion-icon name="ios-settings"></ion-icon>\n\n      </button>\n\n      </div>-->\n\n  <ion-row class="logo-row">\n\n    <ion-col width-67>\n\n      <img class="logo" src="./assets/imgs/logo.png"/>\n\n    </ion-col>\n\n  </ion-row>\n\n    <form (ngSubmit)="login()" #loginForm="ngForm">\n\n      <ion-row>\n\n        <ion-col>\n\n            <ion-item>\n\n              <ion-input type="email" placeholder="Email" name="email" [(ngModel)]="email" required></ion-input>\n\n            </ion-item>\n\n            \n\n            <ion-item>\n\n              <ion-input type="password" placeholder="Password" name="password" [(ngModel)]="password" required></ion-input>\n\n            </ion-item>\n\n          \n\n        </ion-col>\n\n      </ion-row>\n\n      \n\n      <ion-row>\n\n        <ion-col class="signup-col">\n\n          <button ion-button class="submit-btn" full type="submit" [disabled]="!loginForm.form.valid">Login</button>\n\n        </ion-col>\n\n      </ion-row>\n\n      \n\n    </form>\n\n</ion-content>'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 261:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__devices_devices__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(482);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(483);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__devices_devices__["a" /* Devices */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\tabs\tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Map" tabIcon="ios-map"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Devices" tabIcon="ios-list"></ion-tab>\n  <!--<ion-tab [root]="tab3Root" tabTitle="Report" tabIcon="md-analytics"></ion-tab>-->\n  <ion-tab [root]="tab4Root" tabTitle="Profile" tabIcon="ios-contact"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Devices; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__selectedVehicle_selectedVehicle__ = __webpack_require__(354);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Devices = /** @class */ (function () {
    function Devices(navCtrl, app, loadingCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.loadingCtrl = loadingCtrl;
        this.restProvider = restProvider;
        this.search = [];
        this.devices = [];
        this.searchDeviceString = '';
    }
    Devices.prototype.ngOnInit = function () {
        this.showLoading();
        this.getDevices();
    };
    Devices.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: false
        });
        this.loading.present();
    };
    Devices.prototype.getDevices = function () {
        var _this = this;
        this.restProvider.getDevices().retry(3).subscribe(function (device) {
            _this.devices = device;
            _this.search = device;
        });
        this.loading.dismiss();
    };
    Devices.prototype.gotoVehicle = function (e) {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_3__selectedVehicle_selectedVehicle__["a" /* SelectedVehicle */], {
            deviceId: e.id, name: e.name
        });
    };
    Devices.prototype.getItems = function (searchTerm) {
        this.search = this.devices;
        var val = searchTerm.target.value;
        if (val && val.trim() != '') {
            this.search = this.search.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    Devices = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-devices',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\devices\devices.html"*/'<ion-header>\n  <ion-toolbar color="primary">\n    <ion-searchbar  [(ngModel)]="searchDeviceString" showCancelButton="false" (input)="getItems($event)"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n      <ion-item *ngFor="let p of search" (click)="gotoVehicle(p)">\n      <ion-avatar item-start>\n          <img *ngIf="p.status === \'online\'" src="./assets/imgs/online_list.png">\n          <img *ngIf="p.status === \'offline\'" src="./assets/imgs/offline_list.png">\n          <img *ngIf="p.status === \'unknown\'" src="./assets/imgs/offline_list.png">\n      </ion-avatar>\n        <h2>{{p.name}}</h2>\n        <p>{{p.status}}</p>\n      </ion-item>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\devices\devices.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], Devices);
    return Devices;
}());

//# sourceMappingURL=devices.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectedVehicle; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pickerModal_pickerModal__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_config__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SelectedVehicle = /** @class */ (function () {
    function SelectedVehicle(navCtrl, modalCtrl, restProvider, config, authService, navParams, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.restProvider = restProvider;
        this.config = config;
        this.authService = authService;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.devicesMap = new Map();
        this.markersMap = new Map();
        this.markerArray = [];
    }
    SelectedVehicle.prototype.ngOnInit = function () {
        this.deviceId = this.navParams.get('deviceId');
        this.name = this.navParams.get('name');
        this.loadMap();
        this.showLoading();
    };
    SelectedVehicle.prototype.loadMap = function () {
        var _this = this;
        this.mapElement = document.getElementById('track');
        var mapOptions = {
            controls: {
                zoom: true,
                myLocationButton: true
            },
        };
        this.map = __WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["a" /* GoogleMaps */].create(this.mapElement, mapOptions);
        this.map.setMyLocationButtonEnabled(false);
        this.map.setMyLocationEnabled(false);
        // Wait the MAP_READY before using any methods.
        this.map.on(__WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_READY)
            .subscribe(function () {
            _this.getDevice();
            _this.interval = setInterval(function () {
                _this.getDevice();
            }, 500);
        });
    };
    SelectedVehicle.prototype.ngOnDestroy = function () {
        clearInterval(this.interval);
        this.map.clear();
        this.map.remove();
    };
    SelectedVehicle.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: false
        });
    };
    SelectedVehicle.prototype.getDevice = function () {
        var _this = this;
        this.restProvider.getDevices().retry(3).subscribe(function (device) {
            _this.devices = device;
            for (var i = 0; i < device.length; i++) {
                _this.devicesMap.set(device[i].id, _this.devices[i]);
                _this.getPosition(device[i].id, device[i].positionId);
            }
        });
    };
    SelectedVehicle.prototype.getPosition = function (deviceId, id) {
        var _this = this;
        this.restProvider.getPositionById(deviceId, id)
            .then(function (resp) {
            _this.positions = JSON.parse(resp.data);
            _this.loadMarkers(_this.positions);
        }, function (error) {
            //console.log(error);
        });
    };
    SelectedVehicle.prototype.loadMarkers = function (data) {
        var _this = this;
        data.forEach(function (value) {
            if (value.deviceId == _this.deviceId) {
                var markerIcon = void 0;
                var convertedSpeed = void 0;
                var speed = value.speed;
                var factor = 1.852;
                var laterdate = __WEBPACK_IMPORTED_MODULE_7_moment___default()().format();
                var earlierdate = __WEBPACK_IMPORTED_MODULE_7_moment___default()().format(value.fixTime);
                _this.lastUpdate = earlierdate;
                var difference = __WEBPACK_IMPORTED_MODULE_7_moment___default()(laterdate).valueOf() - __WEBPACK_IMPORTED_MODULE_7_moment___default()(earlierdate).valueOf();
                var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
                difference -= daysDifference * 1000 * 60 * 60 * 24;
                var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
                difference -= hoursDifference * 1000 * 60 * 60;
                var minutesDifference = Math.floor(difference / 1000 / 60);
                difference -= minutesDifference * 1000 * 60;
                var secondsDifference = Math.floor(difference / 1000);
                var staticTime = void 0;
                if (daysDifference == 0 && hoursDifference == 0 && minutesDifference == 0) {
                    staticTime = '( ' + secondsDifference + ' sec )';
                }
                else if (daysDifference == 0 && hoursDifference == 0 && minutesDifference != 0) {
                    staticTime = '( ' + minutesDifference + ' min ' + secondsDifference + ' sec )';
                }
                else if (daysDifference == 0 && hoursDifference != 0 && minutesDifference != 0) {
                    staticTime = '( ' + hoursDifference + ' hr ' + minutesDifference + ' min ' + secondsDifference + ' sec )';
                }
                else if (daysDifference != 0 && hoursDifference != 0 && minutesDifference != 0) {
                    staticTime = '( ' + daysDifference + ' days ' + hoursDifference + ' hr ' + minutesDifference + ' min )';
                }
                else if (daysDifference == -1 && hoursDifference == 23 && minutesDifference == 59) {
                    staticTime = '( ' + secondsDifference + ' sec )';
                }
                else {
                    staticTime = '';
                }
                if (speed > 0) {
                    convertedSpeed = (speed * factor).toFixed(0) + " Km/hr";
                }
                else {
                    convertedSpeed = 0;
                }
                if (_this.devicesMap.get(value.deviceId).category != null) {
                    _this.category = _this.devicesMap.get(value.deviceId).category;
                }
                else {
                    _this.category = "default";
                }
                if (_this.devicesMap.get(value.deviceId).status == 'online' && speed < 3) {
                    _this.motionStatus = 'Static ' + staticTime;
                    markerIcon = {
                        url: './assets/imgs/marker/marker_' + _this.category + '_static.png',
                        size: { width: 30, height: 30 }
                    };
                }
                else if (_this.devicesMap.get(value.deviceId).status == 'online' && minutesDifference > 1) {
                    _this.motionStatus = 'Static ' + staticTime;
                    markerIcon = {
                        url: './assets/imgs/marker/marker_' + _this.category + '_static.png',
                        size: { width: 30, height: 30 }
                    };
                }
                else if (_this.devicesMap.get(value.deviceId).status == 'online' && minutesDifference < 1) {
                    _this.motionStatus = 'Moving ' + convertedSpeed;
                    markerIcon = {
                        url: './assets/imgs/marker/marker_' + _this.category + '_online.png',
                        size: { width: 30, height: 30 }
                    };
                }
                else if (_this.devicesMap.get(value.deviceId).status == 'unknown') {
                    _this.motionStatus = 'Static ' + staticTime;
                    markerIcon = {
                        url: './assets/imgs/marker/marker_' + _this.category + '_offline.png',
                        size: { width: 30, height: 30 }
                    };
                }
                else {
                    markerIcon = {
                        url: './assets/imgs/marker/marker_' + _this.category + '_offline.png',
                        size: { width: 30, height: 30 }
                    };
                }
                if (value.address == null) {
                    _this.address = "Loading...";
                }
                else {
                    _this.address = value.address;
                }
                var pos = new __WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["d" /* LatLng */](value.latitude, value.longitude);
                if (_this.marker != null) {
                    _this.map.animateCamera({
                        target: pos,
                        zoom: _this.map.getCameraZoom(),
                        duration: 4
                    }),
                        _this.marker.setPosition(pos);
                    _this.marker.setIcon(markerIcon);
                    _this.marker.setRotation(value.course);
                }
                else {
                    _this.map.animateCamera({
                        target: pos,
                        zoom: 15,
                        duration: 4
                    });
                    _this.deviceMarker = _this.map.addMarker({
                        icon: markerIcon,
                        position: pos,
                        flat: true,
                        rotation: value.course
                    })
                        .then(function (marker) {
                        _this.marker = marker;
                        //this.markerArray.push(marker)
                    });
                }
            }
        });
    };
    SelectedVehicle.prototype.openModal = function () {
        var myModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pickerModal_pickerModal__["a" /* PickerModal */], { deviceId: this.deviceId, name: this.name });
        myModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Navbar */])
    ], SelectedVehicle.prototype, "navBar", void 0);
    SelectedVehicle = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-selectedVehicle',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\selectedVehicle\selectedVehicle.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{name}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n<div #track id="track"></div>\n<ion-card>\n    <ion-card-content> \n    <ion-item>\n      <ion-icon name="locate" item-start medium></ion-icon>\n      <h4>{{name}}</h4>\n      <p>{{address}}</p>\n    </ion-item>\n  \n    <ion-item>\n      <ion-icon name="ios-speedometer-outline" item-left medium></ion-icon>\n      <h4>{{motionStatus}}</h4>\n    </ion-item>\n  \n    <ion-item>\n      <span item-left><h4>{{lastUpdate | date : "dd-MM-yyyy hh:mm:ss"}}</h4></span>\n      <button ion-button icon-left clear item-end (click)="openModal()">\n        <ion-icon name="navigate"></ion-icon>\n        Playback\n      </button>\n    </ion-item>\n    </ion-card-content> \n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\selectedVehicle\selectedVehicle.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_config__["a" /* Config */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */]])
    ], SelectedVehicle);
    return SelectedVehicle;
}());

//# sourceMappingURL=selectedVehicle.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickerModal; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__drawmap_drawmap__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PickerModal = /** @class */ (function () {
    function PickerModal(viewCtrl, navParams, app) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.app = app;
    }
    PickerModal.prototype.closeModal = function () {
        var data = { deviceId: null };
        this.viewCtrl.dismiss(data);
    };
    PickerModal.prototype.ngOnInit = function () {
        this.deviceId = this.navParams.get('deviceId');
        this.deviceName = this.navParams.get('name');
    };
    PickerModal.prototype.show = function () {
        this.closeModal();
        if (this.timeperiod == "today") {
            var fmtDate = __WEBPACK_IMPORTED_MODULE_3_moment___default()().format('YYYY-MM-DD 00:00:00');
            var toDate = __WEBPACK_IMPORTED_MODULE_3_moment___default()().format('YYYY-MM-DD 23:59:59');
            this.startDttm = __WEBPACK_IMPORTED_MODULE_3_moment___default()(fmtDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
            this.endDttm = __WEBPACK_IMPORTED_MODULE_3_moment___default()(toDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
        else if (this.timeperiod == "yesterday") {
            var fmtDate = __WEBPACK_IMPORTED_MODULE_3_moment___default()().subtract(1, "days").format('YYYY-MM-DD 00:00:00');
            var toDate = __WEBPACK_IMPORTED_MODULE_3_moment___default()().subtract(1, "days").format('YYYY-MM-DD 23:59:59');
            this.startDttm = __WEBPACK_IMPORTED_MODULE_3_moment___default()(fmtDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
            this.endDttm = __WEBPACK_IMPORTED_MODULE_3_moment___default()(toDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
        else {
            var fmtDate = __WEBPACK_IMPORTED_MODULE_3_moment___default.a.utc(this.start).format('YYYY-MM-DD HH:mm:ss');
            var toDate = __WEBPACK_IMPORTED_MODULE_3_moment___default.a.utc(this.end).format('YYYY-MM-DD HH:mm:ss');
            this.startDttm = __WEBPACK_IMPORTED_MODULE_3_moment___default()(fmtDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
            this.endDttm = __WEBPACK_IMPORTED_MODULE_3_moment___default()(toDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
        // console.log(this.deviceId);
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_2__drawmap_drawmap__["a" /* DrawMap */], {
            deviceId: this.deviceId, start: this.startDttm, end: this.endDttm, name: this.deviceName
        });
    };
    PickerModal = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pickerModal',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\pickerModal\pickerModal.html"*/'<div class="alert-content">\n\n<ion-header>\n\n  <ion-toolbar style="min-height: 40px;">\n\n    <ion-title>Select Time Period</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="closeModal()">\n\n          <ion-icon item-right name="ios-close-outline"></ion-icon>\n\n      </button>\n\n  </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="guide-modal">\n\n<div>\n\n<form (ngSubmit)="show()" #pickerModal="ngForm">\n\n  <ion-list radio-group name="timeperiod" [(ngModel)]="timeperiod">\n\n    <ion-item>\n\n      <ion-label>Today</ion-label>\n\n      <ion-radio value="today"></ion-radio>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>Yesterday</ion-label>\n\n      <ion-radio value="yesterday"></ion-radio>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>Custom</ion-label>\n\n      <ion-radio value="custom"></ion-radio>\n\n    </ion-item>\n\n  </ion-list>\n\n  <ion-item>\n\n    <ion-label>Start Date time</ion-label>\n\n    <ion-datetime displayFormat="DD-MMM-YY hh:mm A" name="start" [(ngModel)]="start"></ion-datetime>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <ion-label>End Date time</ion-label>\n\n    <ion-datetime displayFormat="DD-MMM-YY hh:mm A" name="end" [(ngModel)]="end"></ion-datetime>\n\n  </ion-item>\n\n<p></p>\n\n<button ion-button round full type="submit" [disabled]="!pickerModal.form.valid">Show</button>\n\n</form>\n\n</div>\n\n</ion-content>'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\pickerModal\pickerModal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], PickerModal);
    return PickerModal;
}());

//# sourceMappingURL=pickerModal.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DrawMap; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__play_back_play_back__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DrawMap = /** @class */ (function () {
    function DrawMap(navCtrl, modalCtrl, navParams, app, loadingCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.app = app;
        this.loadingCtrl = loadingCtrl;
        this.restProvider = restProvider;
        this.items = [];
    }
    DrawMap.prototype.ngOnInit = function () {
        this.showLoading();
        this.start = this.navParams.get('start');
        this.end = this.navParams.get('end');
        this.deviceId = this.navParams.get('deviceId');
        this.name = this.navParams.get('name');
        this.loadReport();
    };
    DrawMap.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    DrawMap.prototype.loadReport = function () {
        this.loadSummary();
        this.loadTrip();
    };
    DrawMap.prototype.loadSummary = function () {
        var _this = this;
        this.restProvider.getSummaryReport(this.deviceId, this.start, this.end)
            .then(function (resp) {
            var summaryData = JSON.parse(resp.data);
            var distance = summaryData[0].distance;
            var averageSpeed = summaryData[0].averageSpeed;
            var maxSpeed = summaryData[0].maxSpeed;
            var factor = 1.852;
            var avgSpeed;
            var maxiSpeed;
            avgSpeed = (averageSpeed * factor).toFixed(0) + " Km/hr";
            maxiSpeed = (maxSpeed * factor).toFixed(0) + " Km/hr";
            var calcDistance = distance / 1000;
            var totalDistance = (calcDistance).toFixed(2) + " Km";
            var hours, minutes;
            hours = Math.floor(summaryData[0].engineHours / 3600000) + "hr";
            minutes = Math.round((summaryData[0].engineHours % 3600000) / 60000) + "Min";
            _this.averageSpeed = avgSpeed;
            _this.distance = totalDistance;
            _this.maxSpeed = maxiSpeed;
            _this.engineHrs = hours + " " + minutes;
        }, function (error) {
            console.log(error);
        });
    };
    DrawMap.prototype.loadTrip = function () {
        var _this = this;
        this.restProvider.getTripReport(this.deviceId, this.start, this.end)
            .then(function (resp) {
            _this.trip = JSON.parse(resp.data);
            for (var i = 0; i < _this.trip.length; i++) {
                _this.trip[i].startTime = __WEBPACK_IMPORTED_MODULE_4_moment___default.a.utc(_this.trip[i].startTime).format('YYYY-MM-DD HH:mm:ss');
                _this.trip[i].endTime = __WEBPACK_IMPORTED_MODULE_4_moment___default.a.utc(_this.trip[i].endTime).format('YYYY-MM-DD HH:mm:ss');
                _this.items.push(_this.trip[i]);
            }
            _this.loading.dismiss();
        });
    };
    DrawMap.prototype.openMapModel = function (deviceId, startTime, endTime) {
        var start = __WEBPACK_IMPORTED_MODULE_4_moment___default()(startTime).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        var end = __WEBPACK_IMPORTED_MODULE_4_moment___default()(endTime).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_3__play_back_play_back__["a" /* PlayBackPage */], { deviceId: deviceId, startTime: start, endTime: end });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Navbar */])
    ], DrawMap.prototype, "navBar", void 0);
    DrawMap = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-drawmap',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\drawmap\drawmap.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{name}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n<!--<div #drawmap id="drawmap"></div>-->\n\n<ion-card>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col col>Distance</ion-col><ion-col col>{{distance}}</ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col>Maximum Speed</ion-col><ion-col col>{{maxSpeed}}</ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col>Engine Hours</ion-col><ion-col col>{{engineHrs}}</ion-col>\n\n    </ion-row> \n\n  </ion-grid>    \n\n  </ion-card>\n\n  <timeline endIcon="call">\n\n    <timeline-item *ngFor="let item of items">\n\n      <ion-card-content>{{item.startTime}}</ion-card-content>\n\n      <ion-card-content>{{item.endTime}}</ion-card-content>\n\n      <button class="btn" (click)=\'openMapModel(item.deviceId, item.startTime, item.endTime)\' clear>\n\n        <ion-icon name="map"></ion-icon>\n\n    </button>\n\n      <ion-card>\n\n        <ion-card-content>\n\n          <p>{{item.startAddress}}</p>\n\n        </ion-card-content>\n\n      </ion-card>\n\n      <ion-card>\n\n      <ion-card-content>\n\n          <p>{{item.endAddress}}</p>\n\n		  </ion-card-content>\n\n      </ion-card>\n\n    </timeline-item>\n\n\n\n  </timeline>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\drawmap\drawmap.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_rest_rest__["a" /* RestProvider */]])
    ], DrawMap);
    return DrawMap;
}());

//# sourceMappingURL=drawmap.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayBackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(42);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PlayBackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PlayBackPage = /** @class */ (function () {
    function PlayBackPage(navCtrl, navParams, loadingCtrl, restProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.restProvider = restProvider;
        this.pos = [];
        this.eventPos = [];
    }
    PlayBackPage.prototype.ngOnInit = function () {
        this.showLoading();
        this.start = this.navParams.get('startTime');
        this.end = this.navParams.get('endTime');
        this.deviceId = this.navParams.get('deviceId');
        this.loadMap();
    };
    PlayBackPage.prototype.ngOnDestroy = function () {
        this.map.clear();
        this.map.remove();
    };
    PlayBackPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    PlayBackPage.prototype.loadMap = function () {
        var _this = this;
        this.mapElement = document.getElementById('playBack');
        var mapOptions = {
            controls: {
                zoom: true,
                myLocationButton: true
            },
        };
        this.map = __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["a" /* GoogleMaps */].create(this.mapElement, mapOptions);
        // Wait the MAP_READY before using any methods.
        this.map.on(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_READY)
            .subscribe(function () {
            _this.loadData();
            _this.loading.dismiss();
        });
    };
    PlayBackPage.prototype.loadData = function () {
        var _this = this;
        this.restProvider.getPositions(this.deviceId, this.start, this.end)
            .then(function (resp) {
            _this.position = JSON.parse(resp.data);
            for (var i = 0; i < _this.position.length; i++) {
                var pos = new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["d" /* LatLng */](_this.position[i].latitude, _this.position[i].longitude);
                _this.pos.push(pos);
            }
            _this.drawMap();
        }, function (error) {
            console.log(error);
        });
    };
    PlayBackPage.prototype.drawMap = function () {
        this.map.moveCamera({
            target: new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["e" /* LatLngBounds */](this.pos),
            zoom: 5
        });
        this.map.addPolyline({
            points: this.pos,
            'color': '#000000',
            'width': 2,
            'geodesic': true
        });
        this.map.addMarker({
            title: "Start",
            position: this.pos[0],
            icon: {
                url: './assets/imgs/pin-start.png',
                size: { width: 30, height: 30 }
            }
        });
        this.map.addMarker({
            title: "Stop",
            position: this.pos[this.pos.length - 1],
            icon: {
                url: './assets/imgs/pin-stop.png',
                size: { width: 30, height: 30 }
            }
        });
        this.loading.dismiss();
    };
    PlayBackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-play-back',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\play-back\play-back.html"*/'<!--\n  Generated template for the PlayBackPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>PlayBack</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div #track id="playBack"></div>\n</ion-content>\n'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\play-back\play-back.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], PlayBackPage);
    return PlayBackPage;
}());

//# sourceMappingURL=play-back.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_http__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(533);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { Http, RequestOptions, Headers } from '@angular/http';




/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = /** @class */ (function () {
    function RestProvider(http, config) {
        this.http = http;
        this.config = config;
    }
    RestProvider.prototype.getDevices = function (options) {
        if (options === void 0) { options = {}; }
        var responseData = this.http.get(this.config.apiUrl + "/api/devices", {}, {})
            .then(function (resp) { return options.responseType == 'text' ? resp.data : JSON.parse(resp.data); });
        return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].fromPromise(responseData);
    };
    RestProvider.prototype.getLivePositions = function (options) {
        if (options === void 0) { options = {}; }
        var headers = {
            'Accept': 'application/json;charset=utf-8'
        };
        var responseData = this.http.get(this.config.apiUrl + "/api/positions", {}, headers)
            .then(function (resp) { return options.responseType == 'text' ? resp.data : JSON.parse(resp.data); });
        return __WEBPACK_IMPORTED_MODULE_2_rxjs__["Observable"].fromPromise(responseData);
    };
    RestProvider.prototype.getPositions = function (deviceId, start, end) {
        var headers = {
            'Accept': 'application/json;charset=utf-8'
        };
        return this.http.get(this.config.apiUrl + "/api/positions?deviceId=" + deviceId + "&from=" + start + "&to=" + end, {}, headers);
    };
    RestProvider.prototype.getPositionById = function (deviceId, id) {
        var headers = {
            'Accept': 'application/json;charset=utf-8'
        };
        return this.http.get(this.config.apiUrl + "/api/positions?deviceId=" + deviceId + "&id=" + id, {}, headers);
    };
    RestProvider.prototype.getSummaryReport = function (deviceId, start, end) {
        var headers = {
            'Accept': 'application/json;charset=utf-8'
        };
        return this.http.get(this.config.apiUrl + "/api/reports/summary?deviceId=" + deviceId + "&from=" + start + "&to=" + end, {}, headers);
    };
    RestProvider.prototype.getEventReport = function (deviceId, start, end) {
        var headers = {
            'Accept': 'application/json;charset=utf-8'
        };
        return this.http.get(this.config.apiUrl + "/api/reports/events?deviceId=" + deviceId + "&from=" + start + "&to=" + end, {}, headers);
    };
    RestProvider.prototype.getTripReport = function (deviceId, start, end) {
        var headers = {
            'Accept': 'application/json;charset=utf-8'
        };
        return this.http.get(this.config.apiUrl + "/api/reports/trips?deviceId=" + deviceId + "&from=" + start + "&to=" + end, {}, headers);
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 482:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl, loadingCtrl, authService, restProvider, storage, platform) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.restProvider = restProvider;
        this.storage = storage;
        this.platform = platform;
    }
    ContactPage.prototype.ngOnInit = function () {
        this.showLoading();
        this.getSession();
    };
    ContactPage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: false
        });
        this.loading.present();
    };
    ContactPage.prototype.getSession = function () {
        var _this = this;
        this.authService.getData().then(function (result) {
            _this.session = result;
            _this.username = _this.session.name;
            _this.getDevice();
        }, function (error) {
            console.log(error);
        });
    };
    ContactPage.prototype.logout = function () {
        localStorage.clear();
        this.storage.set('isLogged', false);
        this.platform.exitApp();
    };
    ContactPage.prototype.getDevice = function () {
        var _this = this;
        this.restProvider.getDevices().retry(3).subscribe(function (device) {
            _this.devices = device;
            var Onlinecount = 0;
            var offlineCount = 0;
            for (var i = 0; i < device.length; i++) {
                var totalDevices = Object.keys(device).length;
                _this.total = totalDevices;
                if (device[i].status == "online") {
                    Onlinecount++;
                    _this.active = Onlinecount;
                }
                else {
                    offlineCount++;
                    _this.inactive = offlineCount;
                }
            }
            _this.loading.dismiss();
        });
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\contact\contact.html"*/'<ion-content padding class="transparent-header">\n  <ion-header>\n    <ion-navbar>\n        <ion-title>Profile</ion-title>\n    </ion-navbar>\n  </ion-header>\n  <!--<div id="profile-bg" [ngStyle]="{\'background-image\': \'url(\' + user.coverImage +\')\'}"></div>-->\n  <div id="content" >\n    <div id="profile-info" padding>\n      <img id="profile-image" src="./assets/imgs/user.png">\n      <h3 id="profile-name">{{username}}</h3>\n    </div>\n    <hr/>\n    <ion-row class="profile-numbers">\n      <ion-col col-4>\n        <p>Total Device</p>\n        <span>{{total}}</span>\n      </ion-col>\n      <ion-col col-4>\n        <p>Active</p>\n        <span  style="color:green">{{active}}</span>\n      </ion-col>\n      <ion-col col-4>\n        <p>Inactive</p>\n        <span  style="color:red">{{inactive}}</span>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col class="signup-col">\n        <button ion-button class="submit-btn" full type="submit" (click)="logout()">Logout</button>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\contact\contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 483:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_background_mode__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_local_notifications__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_config__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, loadingCtrl, authService, alertCtrl, config, backgroundMode, localNotifications, restProvider) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.authService = authService;
        this.alertCtrl = alertCtrl;
        this.config = config;
        this.backgroundMode = backgroundMode;
        this.localNotifications = localNotifications;
        this.restProvider = restProvider;
        this.devicesMap = new Map();
        this.markersMap = new Map();
        this.first = true;
        this.bounds = [];
        this.traffic = false;
        this.satellite = false;
        this.pos = [];
        this.markerArray = [];
    }
    HomePage.prototype.ionViewDidLoad = function () {
        // this.showLoading();
        this.backgroundMode.enable();
        this.email = localStorage.getItem('email');
        this.password = localStorage.getItem('password');
        this.setSession();
    };
    HomePage.prototype.setSession = function () {
        var _this = this;
        this.authService.postData(this.email, this.password, 'login').then(function (result) {
            _this.responseData = result;
            _this.getDevice();
            _this.animatePosition();
            setInterval(function () {
                _this.getDevice();
            }, 500);
            _this.loadMap(result);
        }, function (err) {
            _this.showError(err);
        });
    };
    HomePage.prototype.getDevice = function () {
        var _this = this;
        this.restProvider.getDevices().retry(3).subscribe(function (device) {
            _this.devices = device;
            for (var i = 0; i < device.length; i++) {
                _this.devicesMap.set(device[i].id, _this.devices[i]);
            }
            _this.getPosition();
        });
    };
    HomePage.prototype.animatePosition = function () {
        var _this = this;
        this.restProvider.getLivePositions().retry(3).subscribe(function (position) {
            _this.cameraPosition = position;
            for (var i = 0; i < _this.cameraPosition.length; i++) {
                var pos = new __WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["d" /* LatLng */](_this.cameraPosition[i].latitude.min, _this.cameraPosition[i].longitude.min);
                _this.pos.push(pos);
            }
            _this.map.moveCamera({
                target: new __WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["e" /* LatLngBounds */](_this.pos),
                zoom: 5,
                padding: 50
            });
        });
    };
    HomePage.prototype.getPosition = function () {
        var _this = this;
        this.restProvider.getLivePositions().retry(3).subscribe(function (position) {
            _this.positions = position;
            _this.loadMarkers(position);
        });
    };
    HomePage.prototype.showLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...',
            dismissOnPageChange: false
        });
        this.loading.present();
    };
    HomePage.prototype.loadMap = function (data) {
        var _this = this;
        this.mapElement = document.getElementById('map');
        var mapOptions = {
            controls: {
                zoom: true,
                myLocationButton: true
            },
            camera: {
                target: {
                    lat: data.latitude,
                    lng: data.longitude
                },
                zoom: data.zoom
            }
        };
        this.map = __WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["a" /* GoogleMaps */].create(this.mapElement, mapOptions);
        // Wait the MAP_READY before using any methods.
        this.map.one(__WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_READY)
            .then(function () {
            _this.loading.dismiss();
        });
    };
    HomePage.prototype.trafficLayer = function (event) {
        this.map.setTrafficEnabled(event);
        this.traffic = event;
    };
    HomePage.prototype.satelliteLayer = function (event) {
        if (event == true) {
            this.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].SATELLITE);
        }
        else {
            this.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["c" /* GoogleMapsMapTypeId */].NORMAL);
        }
        this.satellite = event;
    };
    HomePage.prototype.loadMarkers = function (position) {
        var _this = this;
        var i = 0;
        position.forEach(function (value) {
            var pos = new __WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["d" /* LatLng */](value.latitude, value.longitude);
            var markerIcon;
            var convertedSpeed;
            var speed = value.speed;
            var factor = 1.852;
            if (_this.devicesMap.get(value.deviceId).category != null) {
                _this.category = _this.devicesMap.get(value.deviceId).category;
            }
            else {
                _this.category = "default";
            }
            var online = {
                url: './assets/imgs/marker/marker_' + _this.category + '_online.png',
                size: { width: 30, height: 30 }
            };
            var idle = {
                url: './assets/imgs/marker/marker_' + _this.category + '_static.png',
                size: { width: 30, height: 30 }
            };
            var offline = {
                url: './assets/imgs/marker/marker_' + _this.category + '_offline.png',
                size: { width: 30, height: 30 }
            };
            var laterdate = new Date(); // 1st January 2000
            var earlierdate = new Date(value.fixTime); // 13th March 1998
            var difference = laterdate.getTime() - earlierdate.getTime();
            var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
            difference -= daysDifference * 1000 * 60 * 60 * 24;
            var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
            difference -= hoursDifference * 1000 * 60 * 60;
            var minutesDifference = Math.floor(difference / 1000 / 60);
            difference -= minutesDifference * 1000 * 60;
            var secondsDifference = Math.floor(difference / 1000);
            var staticTime;
            if (daysDifference == 0 && hoursDifference == 0 && minutesDifference == 0) {
                staticTime = '( ' + secondsDifference + ' sec )';
            }
            else if (daysDifference == 0 && hoursDifference == 0 && minutesDifference != 0) {
                staticTime = '( ' + minutesDifference + ' min ' + secondsDifference + ' sec )';
            }
            else if (daysDifference == 0 && hoursDifference != 0 && minutesDifference != 0) {
                staticTime = '( ' + hoursDifference + ' hr ' + minutesDifference + ' min ' + secondsDifference + ' sec )';
            }
            else if (daysDifference != 0 && hoursDifference != 0 && minutesDifference != 0) {
                staticTime = '( ' + daysDifference + ' days ' + hoursDifference + ' hr ' + minutesDifference + ' min )';
            }
            else if (daysDifference == -1 && hoursDifference == 23 && minutesDifference == 59) {
                staticTime = '( ' + secondsDifference + ' sec )';
            }
            else {
                staticTime = '';
            }
            if (speed > 0) {
                convertedSpeed = (speed * factor).toFixed(0) + " Km/hr";
            }
            else {
                convertedSpeed = 0;
            }
            if (_this.devicesMap.get(value.deviceId).status == 'online' && speed < 3) {
                _this.motionStatus = 'Static ' + staticTime;
                markerIcon = idle;
            }
            else if (_this.devicesMap.get(value.deviceId).status == 'online' && minutesDifference > 1) {
                _this.motionStatus = 'Static ' + staticTime;
                markerIcon = idle;
            }
            else if (_this.devicesMap.get(value.deviceId).status == 'online' && minutesDifference < 1) {
                _this.motionStatus = 'Moving ' + convertedSpeed;
                markerIcon = online;
            }
            else if (_this.devicesMap.get(value.deviceId).status == 'unknown') {
                _this.motionStatus = 'Static ' + staticTime;
                markerIcon = offline;
            }
            else {
                _this.motionStatus = 'Static ' + staticTime;
                markerIcon = offline;
            }
            if (value.address == null) {
                _this.address = "Loading...";
            }
            else {
                _this.address = value.address;
            }
            if (_this.markerArray[i]) {
                _this.markerArray[i].setPosition(pos);
                _this.markerArray[i].setIcon(markerIcon);
                _this.markerArray[i].setRotation(value.course);
                _this.markerArray[i].setTitle(_this.devicesMap.get(value.deviceId).name);
                _this.markerArray[i].setSnippet("Status : " + _this.motionStatus + "\nAddress : " + value.address);
            }
            else {
                _this.deviceMarker = _this.map.addMarker({
                    icon: markerIcon,
                    title: _this.devicesMap.get(value.deviceId).name,
                    position: pos,
                    flat: true,
                    snippet: "Status : " + _this.motionStatus + "\nAddress : " + value.address,
                    rotation: value.course
                })
                    .then(function (marker) {
                    marker
                        .on(__WEBPACK_IMPORTED_MODULE_0__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (selectedMarker) {
                        _this.map.animateCamera({
                            target: marker.getPosition(),
                            zoom: 15,
                            duration: 1
                        });
                    });
                    _this.marker = marker;
                    _this.markerArray.push(marker);
                });
            }
            i++;
        });
    };
    HomePage.prototype.showError = function (text) {
        this.loading.dismiss();
        var alert = this.alertCtrl.create({
            title: 'Erro Loco',
            subTitle: JSON.stringify(text),
            buttons: ['OK']
        });
        alert.present();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\pages\home\home.html"*/'<ion-content>\n  <div #map id="map">\n      <div class="traffic-buttons">\n          <button (click)="trafficLayer(false)">\n              <img *ngIf="traffic"  id="traffic" src="./assets/imgs/traffic_on.png">\n          </button>\n      </div>\n      <div class="traffic-buttons">\n          <button (click)="trafficLayer(true)">\n              <img  *ngIf="!traffic" id="traffic"  src="./assets/imgs/traffic_off.png">\n          </button>\n      </div>\n\n      <div class="layer-buttons">\n          <button (click)="satelliteLayer(false)">\n              <img *ngIf="satellite"  id="traffic" src="./assets/imgs/satellite_e.png">\n          </button>\n      </div>\n      <div class="layer-buttons">\n          <button (click)="satelliteLayer(true)">\n              <img  *ngIf="!satellite" id="traffic"  src="./assets/imgs/satellite_d.png">\n          </button>\n      </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_config__["a" /* Config */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_background_mode__["a" /* BackgroundMode */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_local_notifications__["a" /* LocalNotifications */],
            __WEBPACK_IMPORTED_MODULE_5__providers_rest_rest__["a" /* RestProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 484:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(489);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 489:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_background_mode__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_local_notifications__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(532);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_http__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(260);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_devices_devices__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_contact_contact__ = __webpack_require__(482);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_home_home__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tabs_tabs__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_selectedVehicle_selectedVehicle__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_pickerModal_pickerModal__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_drawmap_drawmap__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_play_back_play_back__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_status_bar__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_auth_service_auth_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_rest_rest__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_config__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_timeline_timeline__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_devices_devices__["a" /* Devices */],
                __WEBPACK_IMPORTED_MODULE_11__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_pickerModal_pickerModal__["a" /* PickerModal */],
                __WEBPACK_IMPORTED_MODULE_17__pages_play_back_play_back__["a" /* PlayBackPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_selectedVehicle_selectedVehicle__["a" /* SelectedVehicle */],
                __WEBPACK_IMPORTED_MODULE_23__components_timeline_timeline__["a" /* TimelineComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_timeline_timeline__["b" /* TimelineItemComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_timeline_timeline__["c" /* TimelineTimeComponent */],
                __WEBPACK_IMPORTED_MODULE_16__pages_drawmap_drawmap__["a" /* DrawMap */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {
                    tabsHideOnSubPages: true
                }, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_devices_devices__["a" /* Devices */],
                __WEBPACK_IMPORTED_MODULE_11__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_pickerModal_pickerModal__["a" /* PickerModal */],
                __WEBPACK_IMPORTED_MODULE_14__pages_selectedVehicle_selectedVehicle__["a" /* SelectedVehicle */],
                __WEBPACK_IMPORTED_MODULE_17__pages_play_back_play_back__["a" /* PlayBackPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_drawmap_drawmap__["a" /* DrawMap */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_1__ionic_native_background_mode__["a" /* BackgroundMode */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_http__["a" /* HTTP */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["a" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_local_notifications__["a" /* LocalNotifications */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_22__providers_config__["a" /* Config */],
                __WEBPACK_IMPORTED_MODULE_20__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_21__providers_rest_rest__["a" /* RestProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(260);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        var _this = this;
        platform.ready().then(function () {
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\wamp64\www\apps\gpstracking\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\wamp64\www\apps\gpstracking\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Config = /** @class */ (function () {
    function Config() {
        this.apiUrl = "http://185.191.229.133:8082";
    }
    Config = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], Config);
    return Config;
}());

//# sourceMappingURL=config.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_http__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthServiceProvider = /** @class */ (function () {
    function AuthServiceProvider(http, config) {
        this.http = http;
        this.config = config;
    }
    AuthServiceProvider.prototype.postData = function (email, password, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var data = {
                'email': email,
                'password': password
            };
            var headers = {
                'Content-Type': 'application/x-www-form-urlencoded'
            };
            _this.http.post('http://185.191.229.133:8082/api/session', data, headers)
                .then(function (res) {
                resolve(res);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    AuthServiceProvider.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.config.apiUrl + '/api/session', {}, {})
                .then(function (data) {
                resolve(data);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    AuthServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_http__["a" /* HTTP */], __WEBPACK_IMPORTED_MODULE_2__config__["a" /* Config */]])
    ], AuthServiceProvider);
    return AuthServiceProvider;
}());

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 816:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 358,
	"./af.js": 358,
	"./ar": 359,
	"./ar-dz": 360,
	"./ar-dz.js": 360,
	"./ar-kw": 361,
	"./ar-kw.js": 361,
	"./ar-ly": 362,
	"./ar-ly.js": 362,
	"./ar-ma": 363,
	"./ar-ma.js": 363,
	"./ar-sa": 364,
	"./ar-sa.js": 364,
	"./ar-tn": 365,
	"./ar-tn.js": 365,
	"./ar.js": 359,
	"./az": 366,
	"./az.js": 366,
	"./be": 367,
	"./be.js": 367,
	"./bg": 368,
	"./bg.js": 368,
	"./bm": 369,
	"./bm.js": 369,
	"./bn": 370,
	"./bn.js": 370,
	"./bo": 371,
	"./bo.js": 371,
	"./br": 372,
	"./br.js": 372,
	"./bs": 373,
	"./bs.js": 373,
	"./ca": 374,
	"./ca.js": 374,
	"./cs": 375,
	"./cs.js": 375,
	"./cv": 376,
	"./cv.js": 376,
	"./cy": 377,
	"./cy.js": 377,
	"./da": 378,
	"./da.js": 378,
	"./de": 379,
	"./de-at": 380,
	"./de-at.js": 380,
	"./de-ch": 381,
	"./de-ch.js": 381,
	"./de.js": 379,
	"./dv": 382,
	"./dv.js": 382,
	"./el": 383,
	"./el.js": 383,
	"./en-au": 384,
	"./en-au.js": 384,
	"./en-ca": 385,
	"./en-ca.js": 385,
	"./en-gb": 386,
	"./en-gb.js": 386,
	"./en-ie": 387,
	"./en-ie.js": 387,
	"./en-il": 388,
	"./en-il.js": 388,
	"./en-nz": 389,
	"./en-nz.js": 389,
	"./eo": 390,
	"./eo.js": 390,
	"./es": 391,
	"./es-do": 392,
	"./es-do.js": 392,
	"./es-us": 393,
	"./es-us.js": 393,
	"./es.js": 391,
	"./et": 394,
	"./et.js": 394,
	"./eu": 395,
	"./eu.js": 395,
	"./fa": 396,
	"./fa.js": 396,
	"./fi": 397,
	"./fi.js": 397,
	"./fo": 398,
	"./fo.js": 398,
	"./fr": 399,
	"./fr-ca": 400,
	"./fr-ca.js": 400,
	"./fr-ch": 401,
	"./fr-ch.js": 401,
	"./fr.js": 399,
	"./fy": 402,
	"./fy.js": 402,
	"./gd": 403,
	"./gd.js": 403,
	"./gl": 404,
	"./gl.js": 404,
	"./gom-latn": 405,
	"./gom-latn.js": 405,
	"./gu": 406,
	"./gu.js": 406,
	"./he": 407,
	"./he.js": 407,
	"./hi": 408,
	"./hi.js": 408,
	"./hr": 409,
	"./hr.js": 409,
	"./hu": 410,
	"./hu.js": 410,
	"./hy-am": 411,
	"./hy-am.js": 411,
	"./id": 412,
	"./id.js": 412,
	"./is": 413,
	"./is.js": 413,
	"./it": 414,
	"./it.js": 414,
	"./ja": 415,
	"./ja.js": 415,
	"./jv": 416,
	"./jv.js": 416,
	"./ka": 417,
	"./ka.js": 417,
	"./kk": 418,
	"./kk.js": 418,
	"./km": 419,
	"./km.js": 419,
	"./kn": 420,
	"./kn.js": 420,
	"./ko": 421,
	"./ko.js": 421,
	"./ku": 422,
	"./ku.js": 422,
	"./ky": 423,
	"./ky.js": 423,
	"./lb": 424,
	"./lb.js": 424,
	"./lo": 425,
	"./lo.js": 425,
	"./lt": 426,
	"./lt.js": 426,
	"./lv": 427,
	"./lv.js": 427,
	"./me": 428,
	"./me.js": 428,
	"./mi": 429,
	"./mi.js": 429,
	"./mk": 430,
	"./mk.js": 430,
	"./ml": 431,
	"./ml.js": 431,
	"./mn": 432,
	"./mn.js": 432,
	"./mr": 433,
	"./mr.js": 433,
	"./ms": 434,
	"./ms-my": 435,
	"./ms-my.js": 435,
	"./ms.js": 434,
	"./mt": 436,
	"./mt.js": 436,
	"./my": 437,
	"./my.js": 437,
	"./nb": 438,
	"./nb.js": 438,
	"./ne": 439,
	"./ne.js": 439,
	"./nl": 440,
	"./nl-be": 441,
	"./nl-be.js": 441,
	"./nl.js": 440,
	"./nn": 442,
	"./nn.js": 442,
	"./pa-in": 443,
	"./pa-in.js": 443,
	"./pl": 444,
	"./pl.js": 444,
	"./pt": 445,
	"./pt-br": 446,
	"./pt-br.js": 446,
	"./pt.js": 445,
	"./ro": 447,
	"./ro.js": 447,
	"./ru": 448,
	"./ru.js": 448,
	"./sd": 449,
	"./sd.js": 449,
	"./se": 450,
	"./se.js": 450,
	"./si": 451,
	"./si.js": 451,
	"./sk": 452,
	"./sk.js": 452,
	"./sl": 453,
	"./sl.js": 453,
	"./sq": 454,
	"./sq.js": 454,
	"./sr": 455,
	"./sr-cyrl": 456,
	"./sr-cyrl.js": 456,
	"./sr.js": 455,
	"./ss": 457,
	"./ss.js": 457,
	"./sv": 458,
	"./sv.js": 458,
	"./sw": 459,
	"./sw.js": 459,
	"./ta": 460,
	"./ta.js": 460,
	"./te": 461,
	"./te.js": 461,
	"./tet": 462,
	"./tet.js": 462,
	"./tg": 463,
	"./tg.js": 463,
	"./th": 464,
	"./th.js": 464,
	"./tl-ph": 465,
	"./tl-ph.js": 465,
	"./tlh": 466,
	"./tlh.js": 466,
	"./tr": 467,
	"./tr.js": 467,
	"./tzl": 468,
	"./tzl.js": 468,
	"./tzm": 469,
	"./tzm-latn": 470,
	"./tzm-latn.js": 470,
	"./tzm.js": 469,
	"./ug-cn": 471,
	"./ug-cn.js": 471,
	"./uk": 472,
	"./uk.js": 472,
	"./ur": 473,
	"./ur.js": 473,
	"./uz": 474,
	"./uz-latn": 475,
	"./uz-latn.js": 475,
	"./uz.js": 474,
	"./vi": 476,
	"./vi.js": 476,
	"./x-pseudo": 477,
	"./x-pseudo.js": 477,
	"./yo": 478,
	"./yo.js": 478,
	"./zh-cn": 479,
	"./zh-cn.js": 479,
	"./zh-hk": 480,
	"./zh-hk.js": 480,
	"./zh-tw": 481,
	"./zh-tw.js": 481
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 816;

/***/ })

},[484]);
//# sourceMappingURL=main.js.map