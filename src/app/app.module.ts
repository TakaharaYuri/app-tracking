import { NgModule, ErrorHandler } from '@angular/core';
import { BackgroundMode } from '@ionic-native/background-mode'
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { GoogleMaps } from '@ionic-native/google-maps';
import { MyApp } from './app.component';
import { HTTP } from '@ionic-native/http';

import { LoginPage } from '../pages/login/login';
import { Devices } from '../pages/devices/devices';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SelectedVehicle } from '../pages/selectedVehicle/selectedVehicle';
import { PickerModal } from '../pages/pickerModal/pickerModal';
import { DrawMap } from '../pages/drawmap/drawmap';
import { PlayBackPage } from '../pages/play-back/play-back';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { RestProvider } from '../providers/rest/rest';
import { Config } from '../providers/config';
import { TimelineComponent } from '../components/timeline/timeline';
import { TimelineTimeComponent } from '../components/timeline/timeline';
import { TimelineItemComponent } from '../components/timeline/timeline';

@NgModule({
  declarations: [
    MyApp,
    Devices,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    PickerModal,
    PlayBackPage,
    SelectedVehicle,
    TimelineComponent,
    TimelineItemComponent,
    TimelineTimeComponent,
    DrawMap
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages:true
   }),
   IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Devices,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    PickerModal,
    SelectedVehicle,
    PlayBackPage,
    DrawMap
  ],
  providers: [
    BackgroundMode,
    HTTP,
    GoogleMaps,
    StatusBar,
    SplashScreen,
    LocalNotifications,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Config,
    AuthServiceProvider,
    RestProvider
  ]
})
export class AppModule {}
