import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, Platform} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  
  loading:Loading;
  username:any;
  session: any;
  devices: any;
  total:any;
  active:any;
  inactive:any;

  constructor(
    public navCtrl: NavController, 
    private loadingCtrl: LoadingController, 
    private authService: AuthServiceProvider,
    private restProvider: RestProvider,
    public storage: Storage,
    private platform: Platform) {

  }

  ngOnInit(){
    this.showLoading();
    this.getSession();
   }

   showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  getSession(){
    this.authService.getData().then((result) => {
        this.session = result;
        this.username = this.session.name;
        this.getDevice();
      },error =>{
        console.log(error);
      })
  }

  logout(){
    localStorage.clear();
    this.storage.set('isLogged', false);
    this.platform.exitApp();
 }

  getDevice(){
    this.restProvider.getDevices().retry(3).subscribe(device => {
      this.devices = device;
      var Onlinecount = 0;
      var offlineCount = 0;
      for(let i=0; i< device.length;i++){
        var totalDevices = Object.keys(device).length;
        this.total = totalDevices;
        if(device[i].status == "online"){
          Onlinecount++;
          this.active = Onlinecount
        }else{
          offlineCount++;
          this.inactive = offlineCount;
        }
      }
      this.loading.dismiss();
      });
   }

}
