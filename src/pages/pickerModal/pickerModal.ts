import { Component } from '@angular/core';
import { ViewController,NavParams, App } from 'ionic-angular';
import { DrawMap } from '../drawmap/drawmap';
import moment from 'moment';

@Component({
  selector: 'page-pickerModal',
  templateUrl: 'pickerModal.html'
})

export class PickerModal {
  constructor(public viewCtrl: ViewController, public navParams:NavParams, public app:App) {
    
  }

  start:any;
  end:any;
  timeperiod:any;
  startDttm:any;
  endDttm:any;
  deviceId:any;
  deviceName:any;

  closeModal() {
    let data = { deviceId: null };
    this.viewCtrl.dismiss(data);
  }

  ngOnInit(){
    this.deviceId = this.navParams.get('deviceId'); 
    this.deviceName = this.navParams.get('name');
  }

  show(){
   this.closeModal();
   if(this.timeperiod == "today"){
      
      let fmtDate = moment().format('YYYY-MM-DD 00:00:00');
      let toDate = moment().format('YYYY-MM-DD 23:59:59');

      this.startDttm = moment(fmtDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
      this.endDttm = moment(toDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');

   }else if(this.timeperiod == "yesterday"){
    let fmtDate = moment().subtract(1, "days").format('YYYY-MM-DD 00:00:00');
    let toDate = moment().subtract(1, "days").format('YYYY-MM-DD 23:59:59');

    this.startDttm = moment(fmtDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    this.endDttm = moment(toDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');

   }else{
      let fmtDate = moment.utc(this.start).format('YYYY-MM-DD HH:mm:ss');
      let toDate = moment.utc(this.end).format('YYYY-MM-DD HH:mm:ss');
  
      this.startDttm = moment(fmtDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
      this.endDttm = moment(toDate).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
   }
   // console.log(this.deviceId);

   this.app.getRootNav().push(DrawMap, {
    deviceId: this.deviceId, start: this.startDttm, end: this.endDttm, name:this.deviceName
    });
  }

}