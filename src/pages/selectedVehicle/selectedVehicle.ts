import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, Marker, LatLng} from '@ionic-native/google-maps';
import { Component,ViewChild } from '@angular/core';
import { NavController, ModalController, NavParams, Navbar,ViewController, LoadingController, Loading } from 'ionic-angular';
import { PickerModal } from '../pickerModal/pickerModal';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { RestProvider } from '../../providers/rest/rest';
import { Device } from '../../model/model.device';
import { Config } from '../../providers/config'
import moment from 'moment';

@Component({
  selector: 'page-selectedVehicle',
  templateUrl: 'selectedVehicle.html'
})

export class SelectedVehicle {
  @ViewChild(Navbar) navBar: Navbar;
  map: GoogleMap;
  mapElement: HTMLElement;
  markers:Marker;
  positions : any;
  interval: any;
  loading: Loading;
  motionStatus:any;
  lastUpdate:any;
  devices : Device[];
  responseData : any;
  category : any;

  devicesMap : Map<number, Device> = new Map<number, Device>();
  markersMap : Map<number, Marker> = new Map<number, Marker>();
  public markerArray = [];
  public marker;
  public address;
  public deviceMarker: any;
  public deviceId: any;
  public name: any;
  public setInteval : any;
  public viewCtrl: ViewController;
  public inteval : any;

  constructor(
    public navCtrl: NavController, 
    public modalCtrl: ModalController, 
    private restProvider:RestProvider, 
    public config:Config,
    private authService:AuthServiceProvider, 
    public navParams:NavParams, 
    private loadingCtrl: LoadingController) {
  
  }

  ngOnInit(){
    this.deviceId = this.navParams.get('deviceId'); 
    this.name = this.navParams.get('name'); 
    this.loadMap();
    this.showLoading();
  }

  loadMap(){
    this.mapElement = document.getElementById('track');

    let mapOptions: GoogleMapOptions = {
      controls:{
        zoom:true,
        myLocationButton:true
      },
    };

    this.map = GoogleMaps.create(this.mapElement, mapOptions);
    this.map.setMyLocationButtonEnabled(false);
    this.map.setMyLocationEnabled(false);
    // Wait the MAP_READY before using any methods.
    this.map.on(GoogleMapsEvent.MAP_READY)
      .subscribe(() => {
        this.getDevice();
         this.interval = setInterval(() => {
        this.getDevice();
      }, 500);
      });
  }

ngOnDestroy() {
  clearInterval(this.interval);
  this.map.clear();
  this.map.remove();
}

showLoading() {
  this.loading = this.loadingCtrl.create({
    content: 'Please wait...',
    dismissOnPageChange: false
  });
}

getDevice(){
    this.restProvider.getDevices().retry(3).subscribe(device => {
      this.devices = device;
      for(let i=0; i< device.length;i++){
        this.devicesMap.set(device[i].id, this.devices[i]);
        this.getPosition(device[i].id, device[i].positionId);
      }
      });
}

getPosition(deviceId, id){
  this.restProvider.getPositionById(deviceId, id)
  .then((resp) => {
    this.positions = JSON.parse(resp.data);
    this.loadMarkers(this.positions);
  },error =>{
    //console.log(error);
  })
 }

  loadMarkers(data){
    data.forEach(value => {  
      if(value.deviceId == this.deviceId){
        let markerIcon;
        let convertedSpeed;
        let speed = value.speed;
        let factor = 1.852;
  
        let laterdate = moment().format();  
        let earlierdate = moment().format(value.fixTime);
        this.lastUpdate = earlierdate;
        let difference = moment(laterdate).valueOf() - moment(earlierdate).valueOf();
          let daysDifference = Math.floor(difference/1000/60/60/24);
          difference -= daysDifference*1000*60*60*24
    
          let hoursDifference = Math.floor(difference/1000/60/60);
          difference -= hoursDifference*1000*60*60
    
          let minutesDifference = Math.floor(difference/1000/60);
          difference -= minutesDifference*1000*60
    
          let secondsDifference = Math.floor(difference/1000);
    
          let staticTime;
    
          if(daysDifference == 0 && hoursDifference == 0 && minutesDifference == 0){
              staticTime = '( '+secondsDifference + ' sec )';
          }else if(daysDifference == 0 && hoursDifference == 0 && minutesDifference != 0){
             staticTime = '( '+minutesDifference + ' min ' + secondsDifference + ' sec )';
          }else if(daysDifference == 0 && hoursDifference != 0 && minutesDifference != 0) {
              staticTime = '( '+hoursDifference + ' hr ' + minutesDifference + ' min ' + secondsDifference + ' sec )';
          }else if(daysDifference != 0 && hoursDifference != 0 && minutesDifference != 0){
              staticTime = '( '+daysDifference + ' days ' + hoursDifference + ' hr ' + minutesDifference + ' min )';
          }else if(daysDifference == -1 && hoursDifference == 23 && minutesDifference == 59){
              staticTime = '( '+secondsDifference + ' sec )';
          }
          else{
            staticTime = '';
          }

        if(speed > 0){
          convertedSpeed = (speed * factor).toFixed(0)+" Km/hr";
        }
        else{
          convertedSpeed = 0;
        }
  
        if(this.devicesMap.get(value.deviceId).category != null){
          this.category = this.devicesMap.get(value.deviceId).category;
        }else{
          this.category = "default";
        }
        
        if(this.devicesMap.get(value.deviceId).status == 'online' && speed < 3){
          this.motionStatus = 'Static '+staticTime;
          markerIcon =  {
              url: './assets/imgs/marker/marker_'+this.category+'_static.png',
              size: {width: 30,height: 30}
           };
        }
        else if(this.devicesMap.get(value.deviceId).status == 'online' && minutesDifference > 1)
        {
          this.motionStatus = 'Static '+staticTime;
          markerIcon =  {
            url: './assets/imgs/marker/marker_'+this.category+'_static.png',
            size: {width: 30,height: 30}
         };
        }
        else if(this.devicesMap.get(value.deviceId).status == 'online' && minutesDifference < 1){
          this.motionStatus = 'Moving ' +convertedSpeed;
          markerIcon =  {
            url: './assets/imgs/marker/marker_'+this.category+'_online.png',
            size: {width: 30,height: 30}
         };
        }
        else if(this.devicesMap.get(value.deviceId).status == 'unknown'){
          this.motionStatus = 'Static '+staticTime;
          markerIcon =  {
            url: './assets/imgs/marker/marker_'+this.category+'_offline.png',
            size: {width: 30,height: 30}
         };
        }else{
          markerIcon =  {
            url: './assets/imgs/marker/marker_'+this.category+'_offline.png',
            size: {width: 30,height: 30}
         };
        }
        
        if(value.address == null){
          this.address = "Loading...";
        }
        else{
          this.address = value.address;
        }
  
        let pos = new LatLng(value.latitude, value.longitude);
      if(this.marker != null){
        this.map.animateCamera({
          target: pos,
          zoom: this.map.getCameraZoom(),
          duration:4
        }),
        this.marker.setPosition(pos);
        this.marker.setIcon(markerIcon);
        this.marker.setRotation(value.course);
      }else{
          this.map.animateCamera({
            target: pos,
            zoom: 15,
            duration:4
          })
          this.deviceMarker = this.map.addMarker({
            icon: markerIcon,
            position: pos,
            flat:true,
            rotation:value.course
          })
          .then(marker => {
            this.marker = marker
            //this.markerArray.push(marker)
          });
        }
      }
  })
}

openModal() {
  let myModal = this.modalCtrl.create(PickerModal, {deviceId: this.deviceId, name: this.name});
  myModal.present();
}

}
