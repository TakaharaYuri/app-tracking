import { Component,ViewChild } from '@angular/core';
import { NavController, ModalController, NavParams, Navbar, LoadingController, Loading, App } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { PlayBackPage } from '../play-back/play-back';
import moment from 'moment';

@Component({
  selector: 'page-drawmap',
  templateUrl: 'drawmap.html'
})

export class DrawMap {
    @ViewChild(Navbar) navBar: Navbar;
   
    loading: Loading;
    lastUpdate:any;

    start:any;
    end:any;
    deviceId:any;
    name:any;

    distance:any;
    maxSpeed:any;
    engineHrs:any;
    averageSpeed:any;

    trip: any;

    items = [];
  
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams:NavParams,
    public app:App,
    private loadingCtrl: LoadingController,
    private restProvider:RestProvider) {
  
  }

  ngOnInit(){
    this.showLoading();
    this.start = this.navParams.get('start'); 
    this.end = this.navParams.get('end'); 
    this.deviceId = this.navParams.get('deviceId'); 
    this.name = this.navParams.get('name');
    this.loadReport();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  loadReport(){
      this.loadSummary();
      this.loadTrip();
  }

  loadSummary(){
    this.restProvider.getSummaryReport(this.deviceId, this.start, this.end)
    .then((resp) => {
      let summaryData = JSON.parse(resp.data);
      var distance = summaryData[0].distance;
      var averageSpeed = summaryData[0].averageSpeed;
      var maxSpeed = summaryData[0].maxSpeed;
      var factor = 1.852;
      var avgSpeed;
      var maxiSpeed;
      avgSpeed = (averageSpeed * factor).toFixed(0)+ " Km/hr";
      maxiSpeed = (maxSpeed * factor).toFixed(0)+ " Km/hr";
      var calcDistance = distance/1000;
      var totalDistance = (calcDistance).toFixed(2)+ " Km";
      
      var hours, minutes;
        hours = Math.floor(summaryData[0].engineHours / 3600000)+ "hr";
        minutes = Math.round((summaryData[0].engineHours % 3600000) / 60000) + "Min";
    
      this.averageSpeed = avgSpeed;
      this.distance = totalDistance;
      this.maxSpeed = maxiSpeed;
      this.engineHrs = hours+" "+minutes;
      
    },error =>{
      console.log(error);
    })
   }

   loadTrip(){
    this.restProvider.getTripReport(this.deviceId, this.start, this.end)
    .then((resp) => {
      this.trip = JSON.parse(resp.data);
      for(let i=0; i<this.trip.length;i++){
        this.trip[i].startTime = moment.utc(this.trip[i].startTime).format('YYYY-MM-DD HH:mm:ss');
        this.trip[i].endTime = moment.utc(this.trip[i].endTime).format('YYYY-MM-DD HH:mm:ss');
        this.items.push(this.trip[i]);
      }
      this.loading.dismiss();
    });
   }

   openMapModel(deviceId, startTime, endTime){
     let start = moment(startTime).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
     let end = moment(endTime).format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    this.app.getRootNav().push(PlayBackPage, {deviceId: deviceId, startTime: start, endTime:end});
   }
}