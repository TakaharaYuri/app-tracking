import { Component, } from '@angular/core';
import { NavController, Platform, AlertController, LoadingController, Loading } from 'ionic-angular';
import { TabsPage } from '../../pages/tabs/tabs';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  email:any;
  password:any;
  responseData : any;
  loading: Loading;
  url:any;

  constructor(
     private nav: NavController,
     private alertCtrl: AlertController,
     private loadingCtrl: LoadingController,
     private authService:AuthServiceProvider,
     public storage: Storage,
     platform: Platform,
       ) { 
        platform.ready().then(() => {
          this.email = localStorage.getItem('email');
          this.password = localStorage.getItem('password');
          if(this.storage.get('isLogged')){
            this.login();
          }
        });
       }

  ngOnInit(){
    
   }

   ionViewDidLoad() {
   }

  presentUrlConfigModal() {
    let alert = this.alertCtrl.create({
      title: 'Server URL',
      inputs: [
        {
          name: 'url',
          value: this.url,
          placeholder: 'Enter Server URL'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Save', 
          handler: data => {
           localStorage.setItem('url', data.url);
        }
      }
      ]
    });
    alert.present();
  }

 login() {
   console.log('Login');
  // this.showLoading();
  this.authService.postData(this.email,this.password, 'login').then((result) => {
    this.responseData = result;
      localStorage.setItem('email',this.email);
      localStorage.setItem('password',this.password);
      this.storage.set('isLogged', true);
      this.nav.setRoot(TabsPage);
      this.nav.popToRoot();
  }, (err) => {
    // this.showError(JSON.stringify(err));
    
  });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();
 
    let alert = this.alertCtrl.create({
      title: 'Erro Loco',
      subTitle: JSON.stringify(text),
      buttons: ['OK']
    });
    alert.present();
  }
 
}
