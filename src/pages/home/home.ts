import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, Marker, LatLng, LatLngBounds, GoogleMapsMapTypeId} from '@ionic-native/google-maps';
import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, AlertController} from 'ionic-angular';
import { $WebSocket } from 'angular2-websocket/angular2-websocket'
import { BackgroundMode } from '@ionic-native/background-mode';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { RestProvider } from '../../providers/rest/rest';
import { Device } from '../../model/model.device';
import { LivePosition } from '../../model/model.position';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Config } from '../../providers/config'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  devicesMap : Map<number, Device> = new Map<number, Device>();
  markersMap : Map<number, Marker> = new Map<number, Marker>();

  email:any;
  password:any;
  map: GoogleMap;
  mapElement: HTMLElement;
  markers:Marker;
  loading: Loading;
  ws: $WebSocket;
  devices : Device[];
  positions : LivePosition[];
  responseData : any;
  first : boolean = true;
  bounds: LatLng[] = [];
  category: any;
  traffic:boolean = false;
  satellite:boolean = false;
  cameraPosition: LivePosition[];

  public pos = [];
  public markerArray = [];
  public marker;
  public deviceMarker;
  public address;
  public motionStatus;
  public setInteval : any;

  constructor(
    public navCtrl: NavController, 
    private loadingCtrl: LoadingController, 
    private authService:AuthServiceProvider,
    private alertCtrl: AlertController,
    public config:Config,
    public backgroundMode : BackgroundMode,
    public localNotifications: LocalNotifications,
    private restProvider:RestProvider) {

  }

  ionViewDidLoad() {
    // this.showLoading();
    this.backgroundMode.enable();
    this.email = localStorage.getItem('email');
    this.password = localStorage.getItem('password');
    this.setSession();
  }

  setSession(){
    this.authService.postData(this.email,this.password, 'login').then((result) => {
      this.responseData = result;
      this.getDevice();
      this.animatePosition();
      setInterval(() => {
        this.getDevice();
      }, 500);
      this.loadMap(result);
    }, (err) => {
      this.showError(err)
    });
   }

   getDevice(){
    this.restProvider.getDevices().retry(3).subscribe(device => {
      this.devices = device;
      for(let i=0; i< device.length;i++){
        this.devicesMap.set(device[i].id, this.devices[i]);
      }
      this.getPosition();
      });
   }

   animatePosition(){
    this.restProvider.getLivePositions().retry(3).subscribe(position => {
      this.cameraPosition = position;
      for(let i=0; i < this.cameraPosition.length;i++){
        const pos = new LatLng(this.cameraPosition[i].latitude.min, this.cameraPosition[i].longitude.min);      
         this.pos.push(pos);
      }
      this.map.moveCamera({
        target: new LatLngBounds(this.pos),
        zoom: 5,
        padding: 50
      });
      });
   }

   getPosition(){
    this.restProvider.getLivePositions().retry(3).subscribe(position => {
      this.positions = position;
          this.loadMarkers(position);
      });
   }
   showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  loadMap(data){
    this.mapElement = document.getElementById('map');

    let mapOptions: GoogleMapOptions = {
      controls:{
        zoom:true,
        myLocationButton:true
      },
      camera: {
        target: {
          lat: data.latitude,
          lng: data.longitude
        },
        zoom: data.zoom
      }
      
    };

    this.map = GoogleMaps.create(this.mapElement, mapOptions);
    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        this.loading.dismiss();
      });
  }

  trafficLayer(event){
    this.map.setTrafficEnabled(event);
    this.traffic = event;
  }

  satelliteLayer(event){
    if(event == true){
      this.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
    }else{
      this.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
    }
    this.satellite = event;
  }
    
  loadMarkers(position){
    var i = 0;
    position.forEach(value => {  
    let pos = new LatLng(value.latitude, value.longitude);
    let markerIcon;
    let convertedSpeed;
    let speed = value.speed;
    let factor = 1.852;

    if(this.devicesMap.get(value.deviceId).category != null){
      this.category = this.devicesMap.get(value.deviceId).category;
    }else{
      this.category = "default";
    }

    const online = {
      url: './assets/imgs/marker/marker_'+this.category+'_online.png',
      size: {width: 30,height: 30}
   };

    const idle =  {
      url: './assets/imgs/marker/marker_'+this.category+'_static.png',
      size: {width: 30,height: 30}
   };

   const offline = {
      url: './assets/imgs/marker/marker_'+this.category+'_offline.png',
      size: {width: 30,height: 30}
   };

    let laterdate = new Date();     // 1st January 2000
    let earlierdate = new Date(value.fixTime);  // 13th March 1998

    let difference = laterdate.getTime() - earlierdate.getTime();

      let daysDifference = Math.floor(difference/1000/60/60/24);
      difference -= daysDifference*1000*60*60*24

      let hoursDifference = Math.floor(difference/1000/60/60);
      difference -= hoursDifference*1000*60*60

      let minutesDifference = Math.floor(difference/1000/60);
      difference -= minutesDifference*1000*60

      let secondsDifference = Math.floor(difference/1000);

      let staticTime;

      if(daysDifference == 0 && hoursDifference == 0 && minutesDifference == 0){
          staticTime = '( '+secondsDifference + ' sec )';
      }else if(daysDifference == 0 && hoursDifference == 0 && minutesDifference != 0){
         staticTime = '( '+minutesDifference + ' min ' + secondsDifference + ' sec )';
      }else if(daysDifference == 0 && hoursDifference != 0 && minutesDifference != 0) {
          staticTime = '( '+hoursDifference + ' hr ' + minutesDifference + ' min ' + secondsDifference + ' sec )';
      }else if(daysDifference != 0 && hoursDifference != 0 && minutesDifference != 0){
          staticTime = '( '+daysDifference + ' days ' + hoursDifference + ' hr ' + minutesDifference + ' min )';
      }else if(daysDifference == -1 && hoursDifference == 23 && minutesDifference == 59){
          staticTime = '( '+secondsDifference + ' sec )';
      }
      else{
        staticTime = '';
      }

    if(speed > 0){
      convertedSpeed = (speed * factor).toFixed(0)+" Km/hr";
    }
    else{
      convertedSpeed = 0;
    }
    
    if(this.devicesMap.get(value.deviceId).status == 'online' && speed < 3){
      this.motionStatus = 'Static '+staticTime;
      markerIcon = idle;
    }
    else if(this.devicesMap.get(value.deviceId).status == 'online' && minutesDifference > 1)
    {
      this.motionStatus = 'Static '+staticTime;
      markerIcon = idle;
    }
    else if(this.devicesMap.get(value.deviceId).status == 'online' && minutesDifference < 1){
      this.motionStatus = 'Moving ' +convertedSpeed;
      markerIcon =  online;
    }
    else if(this.devicesMap.get(value.deviceId).status == 'unknown'){
      this.motionStatus = 'Static '+staticTime;
      markerIcon = offline;
    }else{
      this.motionStatus = 'Static '+staticTime;
      markerIcon = offline;
    }
    
    if(value.address == null){
      this.address = "Loading...";
    }
    else{
      this.address = value.address;
    }

    if(this.markerArray[i]){
      this.markerArray[i].setPosition(pos);
      this.markerArray[i].setIcon(markerIcon);
      this.markerArray[i].setRotation(value.course);
      this.markerArray[i].setTitle(this.devicesMap.get(value.deviceId).name);
      this.markerArray[i].setSnippet("Status : "+this.motionStatus+"\nAddress : "+value.address);
    }else{
      this.deviceMarker = this.map.addMarker({
        icon: markerIcon,
        title:this.devicesMap.get(value.deviceId).name,
        position: pos,
        flat:true, 
        snippet:"Status : "+this.motionStatus+"\nAddress : "+value.address,
        rotation:value.course
      })
      .then(marker => {
        marker
          .on(GoogleMapsEvent.MARKER_CLICK)
          .subscribe((selectedMarker: any) => {
            this.map.animateCamera({
              target: marker.getPosition(),
              zoom: 15,
              duration:1
            })
          });
        this.marker = marker;
        this.markerArray.push(marker);
      });
    }
    i++;
  });
}

showError(text) {
  this.loading.dismiss();

  let alert = this.alertCtrl.create({
    title: 'Erro Loco',
    subTitle: JSON.stringify(text),
    buttons: ['OK']
  });
  alert.present();
}

}
