import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsEvent, LatLngBounds, LatLng } from '@ionic-native/google-maps';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the PlayBackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-play-back',
  templateUrl: 'play-back.html',
})

export class PlayBackPage {
  map: GoogleMap;
  mapElement: HTMLElement;
  loading: Loading;
  start:any;
  end:any;
  deviceId:any;
  
  position: any;
  public pos = [];
  public eventPos = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private restProvider:RestProvider) {
  }

  ngOnInit(){
    this.showLoading();
    this.start = this.navParams.get('startTime'); 
    this.end = this.navParams.get('endTime'); 
    this.deviceId = this.navParams.get('deviceId');
    this.loadMap();
  }

  ngOnDestroy() {
    this.map.clear();
    this.map.remove();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  loadMap(){
    this.mapElement = document.getElementById('playBack');

    let mapOptions: GoogleMapOptions = {
      controls:{
        zoom:true,
        myLocationButton:true
      },
    };

    this.map = GoogleMaps.create(this.mapElement, mapOptions);
    // Wait the MAP_READY before using any methods.
    this.map.on(GoogleMapsEvent.MAP_READY)
      .subscribe(() => {
        this.loadData();

    this.loading.dismiss();
      });
  }

  loadData(){
    this.restProvider.getPositions(this.deviceId, this.start, this.end)
    .then((resp) => {
      this.position = JSON.parse(resp.data);
      for(let i=0; i < this.position.length;i++){
        const pos = new LatLng(this.position[i].latitude, this.position[i].longitude);      
         this.pos.push(pos);
      }
       this.drawMap()
    },error =>{
      console.log(error);
    })
   }

   drawMap(){
    this.map.moveCamera({
     target: new LatLngBounds(this.pos),
     zoom: 5
   });
   this.map.addPolyline({
     points: this.pos,
     'color' : '#000000',
     'width': 2,
     'geodesic': true
   });

   this.map.addMarker({
      title:"Start",
      position: this.pos[0],
      icon:{
        url: './assets/imgs/pin-start.png',
        size: {width: 30,height: 30}
     }
    });

   this.map.addMarker({
      title:"Stop",
      position: this.pos[this.pos.length - 1],
      icon:{
        url: './assets/imgs/pin-stop.png',
        size: {width: 30,height: 30}
     }
   });
  
    this.loading.dismiss();
   }

}
