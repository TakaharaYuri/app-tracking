import { Component } from '@angular/core';
import { NavController,App, LoadingController, Loading } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { SelectedVehicle } from '../selectedVehicle/selectedVehicle';

@Component({
  selector: 'page-devices',
  templateUrl: 'devices.html'
})
export class Devices {
  
  search = [];
  devices = [];
  searchDeviceString = '';
  loadingPopup;
  loading: Loading;

  constructor(
    public navCtrl: NavController,
    private app: App,
    private loadingCtrl: LoadingController, 
    private restProvider:RestProvider) {

  }

  ngOnInit(){
    this.showLoading();
    this.getDevices();
   }

   showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  getDevices(){
    this.restProvider.getDevices().retry(3).subscribe(device => {
        this.devices = device;
        this.search = device;
     });
      this.loading.dismiss();
  }

  gotoVehicle(e){
      this.app.getRootNav().push(SelectedVehicle, {
        deviceId: e.id, name: e.name
    });
    }   

  getItems(searchTerm){
    this.search =this.devices
    let val = searchTerm.target.value;

    if (val && val.trim() != '') {
        this.search = this.search.filter((item) => {
            return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
  }

}
