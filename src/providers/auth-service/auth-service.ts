import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { Config } from '../config'
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
declare var wkWebView: any;

@Injectable()
export class AuthServiceProvider {

  constructor(public http: HTTP, public config: Config) {
  }

  postData(email, password, type) {
    return new Promise((resolve, reject) => {
      let data = {
        'email': email,
        'password': password
        };
      let headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
      };
      this.http.post('http://185.191.229.133:8082/api/session', data ,headers)
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

    getData() {
      return new Promise((resolve, reject) => {
          this.http.get(this.config.apiUrl+'/api/session', {}, {})
          .then(data =>{
            resolve(data);
          })
          .catch(error => {
            reject(error);
          });
        });
      }

}
