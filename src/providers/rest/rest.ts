//import { Http, RequestOptions, Headers } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Device } from '../../model/model.device';
import { LivePosition } from '../../model/model.position';
import { Config } from '../config'

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class RestProvider {

  constructor(public http: HTTP, public config: Config) {
    
  }

  getDevices(options: any = {}) {
    let responseData = this.http.get(this.config.apiUrl+"/api/devices",{},{})
            .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data));
        return Observable.fromPromise<Device[]>(responseData);
    }

  getLivePositions(options: any = {}){
    let headers = {
      'Accept': 'application/json;charset=utf-8'
  };
    let responseData = this.http.get(this.config.apiUrl+"/api/positions",{}, headers)
    .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data));
    return Observable.fromPromise<LivePosition[]>(responseData);
  }

  getPositions(deviceId, start, end){
    let headers = {
      'Accept': 'application/json;charset=utf-8'
    };
    return this.http.get(this.config.apiUrl+"/api/positions?deviceId="+deviceId+"&from="+start+"&to="+end, {}, headers);
  }

  getPositionById(deviceId, id){
      let headers = {
        'Accept': 'application/json;charset=utf-8'
    };
    return this.http.get(this.config.apiUrl+"/api/positions?deviceId="+deviceId+"&id="+id, {}, headers);
  }

  getSummaryReport(deviceId, start, end){
    let headers = {
      'Accept': 'application/json;charset=utf-8'
    };
    return this.http.get(this.config.apiUrl+"/api/reports/summary?deviceId="+deviceId+"&from="+start+"&to="+end, {}, headers);
  }

  getEventReport(deviceId, start, end){
    let headers = {
      'Accept': 'application/json;charset=utf-8'
    };
    return this.http.get(this.config.apiUrl+"/api/reports/events?deviceId="+deviceId+"&from="+start+"&to="+end, {}, headers);
  }

  getTripReport(deviceId, start, end){
      let headers = {
        'Accept': 'application/json;charset=utf-8'
      };
    return this.http.get(this.config.apiUrl+"/api/reports/trips?deviceId="+deviceId+"&from="+start+"&to="+end, {}, headers);
  }


}
