import { DateTime } from "ionic-angular";

export interface LivePosition {
    accuracy: number;
    address: string;
    altitude: number;
    course: number;
    deviceId: number;
    deviceTime: DateTime;
    fixTime: DateTime;
    id: number;
    latitude: DoubleRange;
    longitude: DoubleRange;
    protocol: string;
    serverTime: DateTime;
    speed: DoubleRange;
  }