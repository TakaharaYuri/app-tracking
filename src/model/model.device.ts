export interface Device {
    id: number;
    name: string;
    uniqueId: string;
    status: string;
    lastUpdate: Date;
    positionId: number;
    phone: string;
    model: string;
    contact: string;
    category: string;
    disabled: boolean;
  }